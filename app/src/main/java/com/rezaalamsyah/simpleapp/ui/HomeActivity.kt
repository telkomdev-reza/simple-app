package com.rezaalamsyah.simpleapp.ui

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.rezaalamsyah.simpleapp.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
    }
}